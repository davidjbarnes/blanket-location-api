import 'dotenv/config';
import cors from 'cors';
import bodyParser from 'body-parser';
import express from 'express';
import models, { sequelize } from './models';
import routes from './routes';

// Setup
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(async (req, res, next) => {
    req.context = { models };
    next();
});

// Routes
app.use('/location', routes.location);
app.get('*', function (req, res) {
    res.redirect('/location');
});

// Start
app.listen(process.env.PORT, () =>
    console.log(`blanket-location-app listening on port ${process.env.PORT}!`),
);