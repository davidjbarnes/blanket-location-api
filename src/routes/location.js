import { Router } from 'express';

const router = Router();

//Create Location
router.post('/', async (req, res) => {
    if (!Array.isArray(req.body))
        return res.send({ success: false, message: "malformed input, expecting an array" });

    let results = [];
    for (let idx in req.body) {
        const name = req.body[idx];
        let location = await req.context.models.Location.findByName(name);
        console.log("loc: " + location);

        if (!location)
            location = await req.context.models.Location.createWithGeoCode(name);
        results.push(location);
    }

    return res.send(results);
});

//Get all Location
router.get('/', async (req, res) => {
    const locations = await req.context.models.Location.findAll();
    return res.send(locations);
});

//Get Location by id
router.get('/:id', async (req, res) => {
    const location = await req.context.models.Location.findById(req.params.id);
    return res.send(location);
});

//Get nearest to given location name
router.get('/nearest/:name', async (req, res) => {
    const name = req.params.name;
    let location = await req.context.models.Location.findByName(name);
    if (!location)
        location = await req.context.models.Location.createWithGeoCode(name);
    const coords = location.geom.coordinates;
    const nearest = await req.context.models.Location.findNearestByCoords(coords);
    return res.send(nearest);
});

export default router;