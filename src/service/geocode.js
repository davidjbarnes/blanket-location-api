import axios from 'axios';

const GOOGLE_API_KEY = process.env.GOOGLE_API_KEY;

export default class GeoCode {
    constructor() { }

    async location(location) {
        const url = 'https://maps.googleapis.com/maps/api/geocode/json?key='.concat(GOOGLE_API_KEY, '&address=', location);
        const response = await axios.get(url);
        const json = response.data;

        if (json.status == "ZERO_RESULTS")
            return null;

        const geometry = json.results[0].geometry.location;
        return {
            name: json.results[0].address_components[0].long_name,
            description: json.results[0].formatted_address,
            geom: { type: 'Point', coordinates: [geometry.lng, geometry.lat], crs: { type: 'name', properties: { name: 'EPSG:4326' } } }
        };
    }
}