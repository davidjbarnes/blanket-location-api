import Sequelize from 'sequelize';

const sequelize = new Sequelize(process.env.DATABASE_URL,
    {
        define: {
            freezeTableName: true,
            underscored: false,
            timestamps: false
        },
        dialect: 'postgres',
    },
);

const models = {
    Location: sequelize.import('./location')
};

export { sequelize };

export default models;