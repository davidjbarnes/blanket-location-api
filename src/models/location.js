import axios from 'axios';
import GeoCode from '../service/geocode';

const geocode = new GeoCode();

const location = (sequelize, DataTypes) => {
    const Location = sequelize.define('location', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            unique: true,
        },
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        geom: DataTypes.GEOMETRY('POINT', 4326)
    });

    Location.findById = async id => {
        let location = await Location.findOne({
            where: { id: id },
        });

        return location;
    };

    Location.findByName = async name => {
        let location = await Location.findOne({
            raw: true,
            where: { name: name },
        });

        return location;
    };

    Location.findNearestByCoords = async coords => {
        //SELECT name FROM location ORDER BY geom <-> ST_GeomFromText ('POINT(-72.685097 41.763710)', 4326) limit 1;
        let query = `SELECT name FROM location ORDER BY geom <-> ST_GeomFromText ('POINT(:longitude :latitude)', 4326) limit 2;`

        return sequelize.query(query, {
            replacements: {
                longitude: coords[0],
                latitude: coords[1]
            },
            raw: true,
            type: sequelize.QueryTypes.SELECT
        });
    };

    Location.createWithGeoCode = async name => {
        const location = await geocode.location(name);
        if (location)
            return await Location.create(location);

        return { success: false };
    };

    return Location;
};

export default location;