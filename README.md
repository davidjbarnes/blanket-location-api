# Blanket Location API

A solution for the Blanket Location API coding challenge. My approach uses a combination of Postgres (PostGIS extension) and Google Geocoding API. Requesting all locaitons queries the location entity. When creating a location, we check the Locaiton entity first. If the Location exists it is returned. If it does not exist we call the Google Geocoding API and then insert the Location object into the Location entity.

To determine nearest Location to another given Location we simply leverage PostGIS geo spatial query such as:

```
SELECT name FROM location ORDER BY geom <-> ST_GeomFromText ('POINT(-72.685097 41.763710)', 4326) limit 1;
```

where the given coordinates represent the coordinates of the "given Locaiton."

## API Endpoints

- Create Location: [POST location](http://blanket-location-api.davidjbarnes.com/location) body: ["Miami","Statue of Liberty",San Diego"...]
- Get all Location: [GET location](http://blanket-location-api.davidjbarnes.com/location)
- Get Location by id: [GET location/:id](http://blanket-location-api.davidjbarnes.com/location/:id)
- Get nearest Location to given Location name: [GET location/nearest/:name](http://blanket-location-api.davidjbarnes.com/nearest/:name)

## Getting Started

Clone the repository:

```
$ git clone https://davidjbarnes@bitbucket.org/davidjbarnes/blanket-location-api.git
```

```
$ npm i && npm run dev
```

### Postgres

Define the Location table:

```
-- Table: public.location

-- DROP TABLE public.location;

CREATE TABLE public.location
(
    name text COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default" NOT NULL,
    geom geometry(Point,4326),
    id integer NOT NULL DEFAULT nextval('location_id_seq'::regclass),
    CONSTRAINT id UNIQUE (id)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.location
    OWNER to ezcfzruledtlov;
```

Insert some data (Miami, FL and New York, NY):

```
INSERT INTO location (name, description, geom)
VALUES ('Miami','Miami is one of the sunniest and the most beautiful cities in Florida, situated at the Atlantic coast.', st_SetSrid(st_MakePoint(-80.191788, 25.761681), 4326));
INSERT INTO location (name, description, geom)
VALUES ('New york','New York City is one of the most known cities-symbols of the USA.', st_SetSrid(st_MakePoint(-73.935242, 40.730610), 4326));
```

Execute query for proximity (Is Hartford CT clostests to New York, NY or Miami, FL?):

```
SELECT name FROM location ORDER BY geom <-> ST_GeomFromText ('POINT(-72.685097 41.763710)', 4326) limit 1;
```

Ensure PostGIS extension is installed:

Via SQL GUI

```
-- DROP EXTENSION postgis;

CREATE EXTENSION postgis
    SCHEMA public
    VERSION "2.5.1";
```

Via Heroku CLI:

```
$ heroku pg:psql
> create extension postgis;
```

## Future Work

- Write tests
- Add tonms of logging
- Add tons of error handling
- Use Sequelize bulkCreate in favor of forEach

## Alternatives

[GeoLib](https://www.npmjs.com/package/geolib) is an excellent alternative approach to using PostGIS for geo spatial queries

## Deployment

```
$ git push heroku master
```

## Hosting

- [Heroku](http://www.heroku.com) - Heroku

## Author

- **David J Barnes** - _Initial work_ - [DavidJBarnes](http://www.davidjbarnes.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
